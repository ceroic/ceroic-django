# ceroic-django

**Important: This image does NOT include Django. You need to include it in your
application requirements**

This docker image includes some helpers and utilities for developing and 
deploying Django applications.

It's based off of ceroic/python-base, which itself is based off of the 
official python image in the Docker Hub. So many of the decisions are based
off of that.

## What else is included?

Along with python we include the following packages and configurations.

## ceroic/ceroic-python:

* nodejs - We typically require node for building client assets.
* phantomjs-prebuilt - Pre-installing this beast makes things go a bit quicker.
* /usr/src/app - Inspired by the Python image, this is where our code goes.

## This Image

* Debian Backports - For updated packages
* nginx - This supports "proxy" mode explained below.
* Init script - This script serves as an entry point, features are explained below.
* Onbuild directives

