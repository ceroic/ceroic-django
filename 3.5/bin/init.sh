#!/usr/bin/env bash
set -e

init_django() {
    cd /usr/src/app/server

    echo "Migrating Database..."
    envdir /etc/env ./manage.py migrate --noinput

    echo "Collecting Static Assets..."
    envdir /etc/env ./manage.py collectstatic --noinput

}

# Django init script that will migrate, collect static, and start the dev
# server.
if [ $# -eq 0 ]
   then
    if [ -f /usr/src/app/server/manage.py ]
    then
        echo "Django Application Found"

        init_django

        echo "Starting Dev Server..."
        exec envdir /etc/env ./manage.py runserver 0.0.0.0:8000
    fi
fi

if [ $1 == "proxy" ]
    then
        echo "Initializing nginx to proxy static files"

        # To use this option apps should have a deploy folder w/ an nginx config
        # in it. Additionally if UWSGI_URL is set as an environment variable,
        # this script will replace UWSGI_URL in the deploy/nginx/default.conf
        # file with it.
        #
        # This allows us to handle decoupling the proxy.

        echo "Initializing proxy with UWSGI_URL: $UWSGI_URL"
        echo "Make sure you are replacing /etc/nginx/sites-available/default with an nginx config"
        sed -i.bak "s#UWSGI_URL#$UWSGI_URL#" /etc/nginx/sites-available/default
        exec nginx -g "daemon off;"
fi

if [ $1 == "uwsgi" ]
    then
        echo "Initializing uwsgi to serve django application"

        if [ ! -f /usr/src/app/server/uwsgi.ini ]; then
            echo "No ini file found. Make sure you uave a uwsgi.ini file in the server folder"
            exit 1
        fi

        init_django
        cd /usr/src/app/server
        exec envdir /etc/env uwsgi uwsgi.ini

fi

exec "$@"
